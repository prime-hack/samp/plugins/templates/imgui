#ifndef IMGUI_IMLOADER_H
#define IMGUI_IMLOADER_H

#include <SRSignal/SRSignal.hpp>

class IDirect3DDevice9;

namespace ImGui {
	class ImLoader {
		IDirect3DDevice9 *_device;
		void *			  _hwnd;

		ImLoader( IDirect3DDevice9 *pDevice, void *hwnd );
		~ImLoader();

		static ImLoader *self;

	public:
		static ImLoader *CreateInstance( IDirect3DDevice9 *pDevice, void *hwnd );
		static void		 DeleteInstance();
		static ImLoader *Instance();
		static ImLoader *get();

		std::tuple<size_t, size_t, size_t> connectToCallbacks( SRSignal<> &invalidDevs, SRSignal<> &createDevs,
															   SRSignal<> &draw );

		SRSignal<> onShutdown;

		void InvalidateDevice();
		void CreateDevice();

		SRSignal<> onInvalidateDevice;
		SRSignal<> onCreateDevice;

		void Draw();

		SRSignal<> onDraw;
	};

} // namespace ImGui

#endif // IMGUI_IMLOADER_H
