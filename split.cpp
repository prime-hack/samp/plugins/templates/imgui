// It's just compile-time tests. You can use library without this file

#include "split.hpp"

#include <array>
#include <deque>
#include <list>
#include <map>

using namespace string_split;
using namespace std::string_view_literals;

static constexpr auto test_string = "Hello World"sv;
static_assert( detail::count_substr( test_string, "l" ) == 3 );
static_assert( detail::count_substr( test_string, "ll" ) == 1 );
static_assert( detail::count_substr( test_string, "" ) == test_string.length() + 1 );

static constexpr auto split_count = detail::count_substr( test_string, "l" ) + 1;
static constexpr auto split_arr = split<std::array<std::string_view, split_count>>( test_string, "l" );
static_assert( split_arr.size() == split_count );
static_assert( split_arr[0] == "He" );
static_assert( split_arr[1] == "" );
static_assert( split_arr[2] == "o Wor" );
static_assert( split_arr[3] == "d" );

[[maybe_unused]] static void compilable() {
    split( test_string, "l" );
    split<std::deque<std::string_view>>( test_string, "l" );
    split<std::list<std::string_view>>( test_string, "l" );
    split<std::map<int, std::string_view>>( test_string, "l" );
}

// SAMP tests
static constexpr auto colored_str = "{FFAACC}hi {FFFFFF}bye\nhello{00CCAA}goodbye{AABBCC}good luck\n{59CA4C}have fun"sv;
static_assert( detail::count_substr( colored_str, "\n" ) == 2 );
static constexpr auto split_count_colored_str = detail::count_substr( colored_str, "\n" ) + 1;
static constexpr auto split_colored_str = split<std::array<std::string_view, split_count_colored_str>>( colored_str, "\n" );
static_assert( split_colored_str.size() == split_count_colored_str );
static_assert( split_colored_str[0] == "{FFAACC}hi {FFFFFF}bye" );
static_assert( split_colored_str[1] == "hello{00CCAA}goodbye{AABBCC}good luck" );
static_assert( split_colored_str[2] == "{59CA4C}have fun" );
