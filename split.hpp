#pragma once

#include <cctype>
#include <string_view>
#include <type_traits>
#include <vector>

namespace string_split {
	namespace detail {
		namespace concepts {
			template<class T, class V>
			concept convertibleTo = std::is_convertible_v<T, V> && requires { static_cast<V>( std::declval<T>() ); };

			template<typename T>
			concept CanFindWithOffset = requires( T str ) { str.find( T(), 0 ); };

			template<typename T>
			concept HaveLength = requires( T str ) { str.length(); };

			template<typename T>
			concept HaveData = requires( T str ) { str.data(); };

			template<typename T>
			concept HaveEmpty = requires( T str ) { str.empty(); };

			template<typename T>
			concept HaveInvalidPos = requires { T::npos; };

			template<typename T, typename V>
			concept HasEmplacable = requires( T arr ) { arr.emplace_back( V() ); };
			template<typename T, typename V>
			concept NotEmplacable = not HasEmplacable<T, V>;

			template<typename T>
			concept Reservable = requires( T arr ) { arr.reserve( 1 ); };
			template<typename T>
			concept NotReservable = not Reservable<T>;

			template<typename T>
			concept StringType = CanFindWithOffset<T> && HaveLength<T> && HaveData<T> && HaveEmpty<T> && HaveInvalidPos<T> &&
								 requires( T str ) { T( str.data(), str.length() ); };

			template<typename T>
			concept ArrayPtr = not HaveLength<T> && requires( T str ) { str[1]; };
		} // namespace concepts

		template<concepts::StringType string_type, concepts::convertibleTo<string_type> substring_type>
		constexpr std::size_t count_substr( const string_type str, const substring_type substr ) {
			std::size_t result = 0;

			std::size_t from = 0;
			while ( true ) {
				auto found = str.find( substr, from );
				if ( found == string_type::npos ) break;
				from = found + 1;
				++result;
			}

			return result;
		}
		template<concepts::StringType string_type, typename pred_fn = int( int )>
		constexpr std::size_t count_pred( const string_type str, const pred_fn &pred ) {
			std::size_t result = 0;

			for ( std::size_t i = 0; i < str.length(); ++i ) {
				if ( !pred( str[i] ) ) continue;
				++result;
			}

			return result;
		}
		template<concepts::StringType string_type, concepts::HasEmplacable<string_type> array_type>
		constexpr void push_string( array_type &array, const auto *data, auto length, std::size_t & ) {
			array.emplace_back( data, length );
		}
		template<concepts::StringType string_type, concepts::NotEmplacable<string_type> array_type>
		constexpr void push_string( array_type &array, const auto *data, auto length, std::size_t &counter ) {
			array[counter++] = string_type{ data, length };
		}
		template<concepts::ArrayPtr string_type> constexpr std::size_t get_next_pos( std::size_t current_pos, const string_type &delim ) {
			std::size_t pos = 0;
			pos = current_pos;
			for ( std::size_t i = 0; delim[i] != 0; ++i ) ++pos;

			return pos;
		}
		template<concepts::HaveLength string_type> constexpr std::size_t get_next_pos( std::size_t current_pos, const string_type &delim ) {
			return current_pos + delim.length();
		}
		template<typename array_type, detail::concepts::StringType string_type, concepts::convertibleTo<string_type> substring_type>
		constexpr void split( array_type &array, const string_type &str, const substring_type &delim ) {
			if ( str.empty() ) return;

			std::size_t push_counter = 0;
			std::size_t from = 0;
			while ( true ) {
				auto found = str.find( delim, from );
				if ( found != string_type::npos )
					detail::push_string<string_type>( array, &str.data()[from], found - from, push_counter );
				else
					detail::push_string<string_type>( array, &str.data()[from], str.length() - from, push_counter );
				if ( found == string_type::npos ) break;
				from = detail::get_next_pos( found, delim );
			}
		}
		template<typename array_type, detail::concepts::StringType string_type, typename pred_fn = int( int )>
		constexpr void split_pred( array_type &array, const string_type &str, pred_fn pred ) {
			if ( str.empty() ) return;

			std::size_t push_counter = 0;
			std::size_t from = 0;
			while ( true ) {
				auto found = string_type::npos;
				for ( auto i = from; i < str.length(); ++i ) {
					if ( !pred( str[i] ) ) continue;
					found = i;
					break;
				}
				if ( found != string_type::npos )
					detail::push_string<string_type>( array, &str.data()[from], found - from, push_counter );
				else
					detail::push_string<string_type>( array, &str.data()[from], str.length() - from, push_counter );
				if ( found == string_type::npos ) break;
				from = found + 1;
			}
		}

	} // namespace detail

	template<detail::concepts::Reservable array_type = std::vector<std::string_view>,
			 detail::concepts::StringType string_type = std::string_view,
			 detail::concepts::convertibleTo<string_type> substring_type = std::string_view>
	constexpr array_type split( const string_type &str, const substring_type delim ) {
		array_type result;
		result.reserve( detail::count_substr( str, delim ) + 1 );

		detail::split( result, str, delim );
		return result;
	}

	template<detail::concepts::Reservable array_type = std::vector<std::string_view>,
			 detail::concepts::StringType string_type = std::string_view,
			 typename pred_fn = int( int )>
	constexpr array_type split_pred( const string_type &str, pred_fn pred ) {
		array_type result;
		result.reserve( detail::count_pred( str, pred ) + 1 );

		detail::split_pred( result, str, pred );
		return result;
	}

	template<detail::concepts::NotReservable array_type,
			 detail::concepts::StringType string_type = std::string_view,
			 detail::concepts::convertibleTo<string_type> substring_type = std::string_view>
	constexpr array_type split( const string_type &str, const substring_type delim ) {
		array_type result;

		detail::split( result, str, delim );
		return result;
	}

	template<detail::concepts::NotReservable array_type,
			 detail::concepts::StringType string_type = std::string_view,
			 typename pred_fn = int( int )>
	constexpr array_type split_spaces( const string_type &str, pred_fn pred ) {
		array_type result;

		detail::split_pred( result, str, pred );
		return result;
	}
} // namespace string_split
