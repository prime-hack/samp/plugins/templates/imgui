#include "ImLoader.h"
#include <d3d9.h>
#include <imgui.h>

#include "imgui/backends/imgui_impl_dx9.h"
#include "imgui/backends/imgui_impl_win32.h"

static WNDPROC	 hOrigProcImGui		   = nullptr;
static bool &	 isMenuOpened		   = *reinterpret_cast<bool *>( 0xBA67A4 );
ImGui::ImLoader *ImGui::ImLoader::self = nullptr;

extern LRESULT ImGui_ImplWin32_WndProcHandler( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
LRESULT WINAPI WndProcImGui( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam ) {
	if ( !isMenuOpened ) {
		if ( uMsg == WM_CHAR ) {
			wchar_t wch;
			MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, (char *)&wParam, 1, &wch, 1 );
			ImGui::GetIO().AddInputCharacter( wch );
		} else if ( ImGui_ImplWin32_WndProcHandler( hwnd, uMsg, wParam, lParam ) )
			return CallWindowProc( hOrigProcImGui, hwnd, 0, 0, 0 );
	}
	return CallWindowProc( hOrigProcImGui, hwnd, uMsg, wParam, lParam );
}

ImGui::ImLoader::ImLoader( IDirect3DDevice9 *pDevice, void *hwnd ) : _device( pDevice ), _hwnd( hwnd ) {
	IMGUI_CHECKVERSION();
	hOrigProcImGui = (WNDPROC)SetWindowLongA( (HWND)hwnd, GWL_WNDPROC, (LONG)WndProcImGui );
	ImGui::CreateContext();
	ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NoMouseCursorChange;
	ImGui_ImplWin32_Init( _hwnd );
	ImGui_ImplDX9_Init( _device );
	ImGui::StyleColorsLight();
}

ImGui::ImLoader::~ImLoader() {
	onShutdown();
	ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
	SetWindowLongA( (HWND)_hwnd, GWL_WNDPROC, (LONG)hOrigProcImGui );
}

ImGui::ImLoader *ImGui::ImLoader::CreateInstance( IDirect3DDevice9 *pDevice, void *hwnd ) {
	if ( self ) return self;
	self = new ImLoader( pDevice, hwnd );
	return self;
}

void ImGui::ImLoader::DeleteInstance() {
	delete self;
	self = nullptr;
}

ImGui::ImLoader *ImGui::ImLoader::Instance() {
	return self;
}

ImGui::ImLoader *ImGui::ImLoader::get() {
	return Instance();
}

std::tuple<size_t, size_t, size_t> ImGui::ImLoader::connectToCallbacks( SRSignal<> &invalidDevs, SRSignal<> &createDevs,
																		SRSignal<> &draw ) {
	auto iid = invalidDevs += std::tuple{ this, &ImLoader::InvalidateDevice };
	auto cid = createDevs += std::tuple{ this, &ImLoader::CreateDevice };
	auto did = draw += std::tuple{ this, &ImLoader::Draw };

	return std::tuple{ iid, cid, did };
}

void ImGui::ImLoader::InvalidateDevice() {
	onInvalidateDevice();
	ImGui_ImplDX9_InvalidateDeviceObjects();
}

void ImGui::ImLoader::CreateDevice() {
	ImGui_ImplDX9_CreateDeviceObjects();
	onCreateDevice();
}

void ImGui::ImLoader::Draw() {
	if ( !isMenuOpened ) {
		ImGui_ImplDX9_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();
		onDraw();
		ImGui::EndFrame();
		ImGui::Render();
		ImGui_ImplDX9_RenderDrawData( ImGui::GetDrawData() );
	}
}
