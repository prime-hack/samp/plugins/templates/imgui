#include "ImWithEventEdited.h"

bool ImWithEventEdited::isEdited() { return __event_isEdited; }

void ImWithEventEdited::processEvent() {
	__event_isEdited = false;
	if ( ImGui::IsItemEdited() ) {
		onEdited();
		__event_isEdited = true;
	}
}
