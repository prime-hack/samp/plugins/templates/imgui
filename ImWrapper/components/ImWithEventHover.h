#ifndef IMWITHEVENTHOVER_H
#define IMWITHEVENTHOVER_H

#include "ImWithEvent.h"

class ImWithEventHover : public virtual ImWithEvent
{
	bool __event_isHovered;
	bool __event_isHoveredWhenBlockedByPopup;
	bool __event_isHoveredWhenBlockedByActiveItem;
	bool __event_isHoveredWhenOverlapped;
	bool __event_isHoveredRectOnly;
public:
	virtual bool isHovered();
	virtual bool isHoveredWhenBlockedByPopup();
	virtual bool isHoveredWhenBlockedByActiveItem();
	virtual bool isHoveredWhenOverlapped();
	virtual bool isHoveredRectOnly();

	SRSignal<> onHovered;
	SRSignal<> onHoveredWhenBlockedByPopup;
	SRSignal<> onHoveredWhenBlockedByActiveItem;
	SRSignal<> onHoveredWhenOverlapped;
	SRSignal<> onHoveredRectOnly;

	SRSignal<> onNotHovered;
	SRSignal<> onNotHoveredWhenBlockedByPopup;
	SRSignal<> onNotHoveredWhenBlockedByActiveItem;
	SRSignal<> onNotHoveredWhenOverlapped;
	SRSignal<> onNotHoveredRectOnly;

protected:
	void processEvent() override;
};

#endif // IMWITHEVENTHOVER_H
