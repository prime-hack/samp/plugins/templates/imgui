#include "ImWithEventClicked.h"

bool ImWithEventClicked::isClicked( int mouse_button ) {
	mouse_button = VkMouseButtonToImGuiMouseButton( mouse_button );
	if ( mouse_button == -1 ) return false;
	return __event_isClicked[mouse_button];
}

void ImWithEventClicked::processEvent() {
	for ( int i = 0; i < countMouseButtons; ++i ) {
		__event_isClicked[i] = false;
		if ( ImGui::IsItemClicked( i ) ) {
			onClicked( ImGuiMouseButtonToVkMouseButton( i ) );
			__event_isClicked[i] = true;
		}
	}
}

int ImWithEventClicked::ImGuiMouseButtonToVkMouseButton( int mouse_button ) {
	if ( mouse_button < 2 )
		return mouse_button + 1;
	else if ( mouse_button > 2 )
		return mouse_button + 3;
	else if ( mouse_button == 2 )
		return mouse_button + 2;
	return -1;
}

int ImWithEventClicked::VkMouseButtonToImGuiMouseButton( int mouse_button ) {
	if ( mouse_button == 0 )
		return -1;
	else if ( mouse_button < 3 )
		return mouse_button - 1;
	else if ( mouse_button > 4 )
		return mouse_button - 3;
	else if ( mouse_button == 4 )
		return mouse_button - 2;
	return -1;
}
