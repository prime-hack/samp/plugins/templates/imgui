#ifndef IMWITHALLEVENTS_H
#define IMWITHALLEVENTS_H

#include "ImWithEventHover.h"
#include "ImWithEventActive.h"
#include "ImWithEventFocus.h"
#include "ImWithEventClicked.h"
#include "ImWithEventVisible.h"
#include "ImWithEventEdited.h"
#include "ImWithEventResize.h"

class ImWithAllEvents
		: public ImWithEventHover,
		public ImWithEventActive,
		public ImWithEventFocus,
		public ImWithEventClicked,
		public ImWithEventVisible,
		public ImWithEventEdited,
		public ImWithEventResize
{
protected:
	void processEvent() override;
};

#endif // IMWITHALLEVENTS_H
