#include "ImWithEventActive.h"

bool ImWithEventActive::isActive() { return __event_isActive; }

bool ImWithEventActive::isInactive() { return __event_isActive ^ true; }

void ImWithEventActive::processEvent() {
	if ( ImGui::IsItemActive() ) {
		if ( !__event_isActive ) onActiveToggle( true );
		onActive();
		__event_isActive = true;
	} else {
		if ( __event_isActive ) onActiveToggle( false );
		onInactive();
		__event_isActive = false;
	}
}
