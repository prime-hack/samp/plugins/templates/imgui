#ifndef IMWITHTOOLTIP_H
#define IMWITHTOOLTIP_H

#include "../../imgui/imgui.h"
#include <string>

class ImWithTooltip {
public:
	virtual ~ImWithTooltip() {}

	std::string tooltip;

protected:
	virtual void processTooltip();
};

#endif // IMWITHTOOLTIP_H
