#include "ImWithTooltip.h"

void ImWithTooltip::processTooltip() {
	if ( !tooltip.empty() && ImGui::IsItemHovered() ) ImGui::SetTooltip( "%s", tooltip.data() );
}
