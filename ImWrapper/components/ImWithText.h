#ifndef IMWITHTEXT_H
#define IMWITHTEXT_H

#include <string>
#include <string_view>

class ImWithText
{
public:
	ImWithText( std::string_view str );
	virtual ~ImWithText(){}

	std::string text;
};

#endif // IMWITHTEXT_H
