#include "ImWithEventHover.h"

bool ImWithEventHover::isHovered() { return __event_isHovered; }

bool ImWithEventHover::isHoveredWhenBlockedByPopup() { return __event_isHoveredWhenBlockedByPopup; }

bool ImWithEventHover::isHoveredWhenBlockedByActiveItem() { return __event_isHoveredWhenBlockedByActiveItem; }

bool ImWithEventHover::isHoveredWhenOverlapped() { return __event_isHoveredWhenOverlapped; }

bool ImWithEventHover::isHoveredRectOnly() { return __event_isHoveredRectOnly; }

void ImWithEventHover::processEvent() {
	__event_isHovered = false;
	__event_isHoveredWhenBlockedByPopup = false;
	__event_isHoveredWhenBlockedByActiveItem = false;
	__event_isHoveredWhenOverlapped = false;
	__event_isHoveredRectOnly = false;

	if ( ImGui::IsItemHovered() ) {
		__event_isHovered = true;
		onHovered();
	} else
		onNotHovered();

	if ( ImGui::IsItemHovered( ImGuiHoveredFlags_AllowWhenBlockedByPopup ) ) {
		__event_isHoveredWhenBlockedByPopup = true;
		onHoveredWhenBlockedByPopup();
	} else
		onNotHoveredWhenBlockedByPopup();

	if ( ImGui::IsItemHovered( ImGuiHoveredFlags_AllowWhenBlockedByActiveItem ) ) {
		__event_isHoveredWhenBlockedByActiveItem = true;
		onHoveredWhenBlockedByActiveItem();
	} else
		onNotHoveredWhenBlockedByActiveItem();

	if ( ImGui::IsItemHovered( ImGuiHoveredFlags_AllowWhenOverlapped ) ) {
		__event_isHoveredWhenOverlapped = true;
		onHoveredWhenOverlapped();
	} else
		onNotHoveredWhenOverlapped();

	if ( ImGui::IsItemHovered( ImGuiHoveredFlags_RectOnly ) ) {
		__event_isHoveredRectOnly = true;
		onHoveredRectOnly();
	} else
		onNotHoveredRectOnly();
}
