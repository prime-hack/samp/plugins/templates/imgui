#ifndef IMWITHEVENTVISIBLE_H
#define IMWITHEVENTVISIBLE_H

#include "ImWithEvent.h"

class ImWithEventVisible : public ImWithEvent {
	bool __event_isVisible;

public:
	virtual bool isVisible();
	virtual bool isInvisible();

	SRSignal<>		 onVisible;
	SRSignal<>		 onInvisible;
	SRSignal< bool > onVisibleToggle;

protected:
	void processEvent() override;
};

#endif // IMWITHEVENTVISIBLE_H
