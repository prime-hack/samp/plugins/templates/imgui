#ifndef IMWITHEVENTACTIVE_H
#define IMWITHEVENTACTIVE_H

#include "ImWithEvent.h"

class ImWithEventActive : public virtual ImWithEvent
{
	bool __event_isActive;
public:
	virtual bool isActive();
	virtual bool isInactive();

	SRSignal<> onActive;
	SRSignal<> onInactive;
	SRSignal<bool> onActiveToggle;

protected:
	void processEvent() override;
};

#endif // IMWITHEVENTACTIVE_H
