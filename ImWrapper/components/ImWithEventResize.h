#ifndef IMWITHEVENTRESIZE_H
#define IMWITHEVENTRESIZE_H

#include "ImWithEvent.h"

class ImWithEventResize : public ImWithEvent
{
	ImVec2 __event_size;
	ImVec2 __event_sizeMin;
	ImVec2 __event_sizeMax;
public:
	virtual ImVec2 itemSize();
	virtual ImVec2 itemSizeMin();
	virtual ImVec2 itemSizeMax();

	SRSignal<ImVec2> onItemSizeChanged;
	SRSignal<ImVec2> onItemSizeMinChanged;
	SRSignal<ImVec2> onItemSizeMaxChanged;

protected:
	void processEvent() override;
};

#endif // IMWITHEVENTRESIZE_H
