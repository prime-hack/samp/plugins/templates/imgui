#ifndef IMWITHEVENTEDITED_H
#define IMWITHEVENTEDITED_H

#include "ImWithEvent.h"

class ImWithEventEdited : public ImWithEvent
{
	bool __event_isEdited;
public:
	virtual bool isEdited();

	SRSignal<> onEdited;

protected:
	void processEvent() override;
};

#endif // IMWITHEVENTEDITED_H
