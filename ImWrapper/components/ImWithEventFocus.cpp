#include "ImWithEventFocus.h"

bool ImWithEventFocus::isFocus() { return __event_isFocus; }

bool ImWithEventFocus::isInFocus() { return __event_isFocus ^ true; }

void ImWithEventFocus::processEvent() {
	if ( ImGui::IsItemFocused() ) {
		if ( !__event_isFocus ) onFocusToggle( true );
		onFocused();
		__event_isFocus = true;
	} else {
		if ( __event_isFocus ) onFocusToggle( false );
		onNotFocused();
		__event_isFocus = false;
	}
}
