#include "ImWithEventResize.h"

ImVec2 ImWithEventResize::itemSize() { return __event_size; }

ImVec2 ImWithEventResize::itemSizeMin() { return __event_sizeMin; }

ImVec2 ImWithEventResize::itemSizeMax() { return __event_sizeMax; }

void ImWithEventResize::processEvent() {
	ImVec2 size = ImGui::GetItemRectSize();
	ImVec2 sizeMin = ImGui::GetItemRectMin();
	ImVec2 sizeMax = ImGui::GetItemRectMax();

	if ( size.x != __event_size.x || size.y != __event_size.y ) onItemSizeChanged( size );
	if ( sizeMin.x != __event_sizeMin.x || sizeMin.y != __event_sizeMin.y ) onItemSizeMinChanged( sizeMin );
	if ( sizeMax.x != __event_sizeMax.x || sizeMax.y != __event_sizeMax.y ) onItemSizeMaxChanged( sizeMax );

	__event_size = size;
	__event_sizeMin = sizeMin;
	__event_sizeMax = sizeMax;
}
