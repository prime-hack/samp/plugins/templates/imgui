#include "ImWithAllEvents.h"

void ImWithAllEvents::processEvent() {
	ImWithEventHover::processEvent();
	ImWithEventActive::processEvent();
	ImWithEventFocus::processEvent();
	ImWithEventClicked::processEvent();
	ImWithEventVisible::processEvent();
	ImWithEventEdited::processEvent();
	ImWithEventResize::processEvent();
}
