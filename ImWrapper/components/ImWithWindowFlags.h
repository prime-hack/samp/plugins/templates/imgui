#ifndef IMWITHWINDOWFLAGS_H
#define IMWITHWINDOWFLAGS_H

#include "../../imgui/imgui.h"

class ImWithWindowFlags {
public:
	virtual ~ImWithWindowFlags() {}

	ImGuiWindowFlags flags = 0;
};

#endif // IMWITHWINDOWFLAGS_H
