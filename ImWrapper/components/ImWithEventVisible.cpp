#include "ImWithEventVisible.h"

bool ImWithEventVisible::isVisible() {
	return __event_isVisible;
}

bool ImWithEventVisible::isInvisible() {
	return __event_isVisible ^ true;
}

void ImWithEventVisible::processEvent() {
	if ( ImGui::IsItemVisible() ) {
		if ( !__event_isVisible ) onVisibleToggle( true );
		onVisible();
		__event_isVisible = true;
	} else {
		if ( __event_isVisible ) onVisibleToggle( false );
		onInvisible();
		__event_isVisible = false;
	}
}
