#ifndef IMWITHNODEFLAGS_H
#define IMWITHNODEFLAGS_H

#include "../../imgui/imgui.h"

class ImWithNodeFlags {
public:
	virtual ~ImWithNodeFlags() {}

	ImGuiTreeNodeFlags flags = 0;
};

#endif // IMWITHNODEFLAGS_H
