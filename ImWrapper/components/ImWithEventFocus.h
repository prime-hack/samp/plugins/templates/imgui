#ifndef IMWITHEVENTFOCUS_H
#define IMWITHEVENTFOCUS_H

#include "ImWithEvent.h"

class ImWithEventFocus : public virtual ImWithEvent
{
	bool __event_isFocus;
public:
	virtual bool isFocus();
	virtual bool isInFocus();

	SRSignal<> onFocused;
	SRSignal<> onNotFocused;
	SRSignal<bool> onFocusToggle;

protected:
	void processEvent() override;
};

#endif // IMWITHEVENTFOCUS_H
