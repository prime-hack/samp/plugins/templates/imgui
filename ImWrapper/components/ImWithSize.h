#ifndef IMWITHSIZE_H
#define IMWITHSIZE_H

#include "../../imgui/imgui.h"

class ImWithSize {
public:
	virtual ~ImWithSize() {}

	ImVec2 size = {0, 0};
};

#endif // IMWITHSIZE_H
