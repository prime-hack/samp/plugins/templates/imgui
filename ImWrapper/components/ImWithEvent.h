#ifndef IMWITHEVENT_H
#define IMWITHEVENT_H

#include "../../imgui/imgui.h"
#include <SRSignal/SRSignal.hpp>

class ImWithEvent {
public:
	virtual ~ImWithEvent() {}

protected:
	virtual void processEvent() = 0;
};

#endif // IMWITHEVENT_H
