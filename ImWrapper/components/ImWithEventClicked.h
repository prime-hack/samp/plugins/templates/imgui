#ifndef IMWITHEVENTCLICKED_H
#define IMWITHEVENTCLICKED_H

#include "ImWithEvent.h"

class ImWithEventClicked : public virtual ImWithEvent
{
	bool __event_isClicked[5];
public:
	virtual bool isClicked(int mouse_button = 1);

	SRSignal<int> onClicked;

	static const int countMouseButtons = 5;

protected:
	void processEvent() override;

	virtual int ImGuiMouseButtonToVkMouseButton(int mouse_button);
	virtual int VkMouseButtonToImGuiMouseButton(int mouse_button);
};

#endif // IMWITHEVENTCLICKED_H
