#ifndef IMCHIELDSBASE_H
#define IMCHIELDSBASE_H

#include "ImBase.h"
#include <deque>

class ImChieldsBase : public ImBase
{
public:
	std::deque<ImBase*> chields;

protected:
	void draw() override;
};

#endif // IMCHIELDSBASE_H
