#include "ImInputText.h"

ImInputText::ImInputText( std::string_view name, std::string_view text, size_t limit )
	: _limit( limit ), name( name ) {
	if ( text.length() > _limit ) _limit = text.length() + 1;
	_buffer = new char[_limit];
	strncpy( _buffer, text.data(), text.length() );
}

ImInputText::~ImInputText() { delete[] _buffer; }

void ImInputText::resize( size_t size ) {
	auto _new = new char( size );
	memcpy( _new, _buffer, ( size > _limit ? _limit : size ) );
	_new[size - 1] = 0;
	delete[] _buffer;
	_buffer = _new;
}

std::string_view ImInputText::text() {
	return _buffer;
}

void ImInputText::setText( std::string_view text ) {
	if ( text.length() > _limit ) resize( text.length() + 1 );
	strncpy( _buffer, text.data(), text.length() );
	onTextChanged( _buffer );
}

void ImInputText::draw() {
	std::string text = _buffer;
	ImGui::InputText( name.data(), _buffer, _limit, flags );
	if ( text != _buffer ) {
		onTextEdited( _buffer );
		onTextChanged( _buffer );
	}
	processEvent();
	processTooltip();
	if ( isActive() && isFocus() )
		if ( ImGui::IsKeyPressed( ImGuiKey_Enter, false ) ) onReturnPressed();
}
