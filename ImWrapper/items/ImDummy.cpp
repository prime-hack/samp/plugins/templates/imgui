#include "ImDummy.h"

ImDummy::ImDummy( const ImVec2 &sz ) { size = sz; }

ImDummy::ImDummy( const float &x, const float &y ) { ImDummy( ImVec2( {x, y} ) ); }

void ImDummy::draw() { ImGui::Dummy( size ); }
