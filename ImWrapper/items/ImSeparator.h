#ifndef IMSEPARATOR_H
#define IMSEPARATOR_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithTooltip.h"

class ImSeparator : public ImBase, public ImWithAllEvents, public ImWithTooltip
{
protected:
	void draw() override;
};

#endif // IMSEPARATOR_H
