#ifndef IMINPUTINT_H
#define IMINPUTINT_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithTooltip.h"

class ImInputInt : public ImBase, public ImWithAllEvents, public ImWithTooltip
{
	int _value;
	int *_pValue = nullptr;
public:
	ImInputInt( std::string_view name, const int &v = 0 );
	ImInputInt( std::string_view name, int *v );

	virtual void setPointer(int *pValue);

	virtual int value();
	virtual void setValue(const int &v);

	int step = 1;
	int fastStep = 100;
	int min = 0x80000000;
	int max = 0x7FFFFFFF;
	std::string name;
	ImGuiInputTextFlags flags = 0;

	SRSignal<int> onIntEdited;
	SRSignal<int> onIntChanged;
	SRSignal<> onReturnPressed;

protected:
	void draw() override;
};

#endif // IMINPUTINT_H
