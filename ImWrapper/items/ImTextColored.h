#ifndef IMTEXTCOLORED_H
#define IMTEXTCOLORED_H

#include "ImText.h"

class ImTextColored : public ImText
{
public:
	ImTextColored( std::string_view str, const size_t &clr = 0xFFFF8000 );

	size_t color;

	static std::deque<ImBase *> fromFormatedString( const std::string &str, size_t clr = 0xFFFF8000 );

protected:
	void draw() override;
};

#endif // IMTEXTCOLORED_H
