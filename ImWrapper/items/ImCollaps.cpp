#include "ImCollaps.h"

ImCollaps::ImCollaps( std::string_view title ) : title( std::string( title ) + "\0" ) {}

void ImCollaps::draw() {
	if ( ImGui::CollapsingHeader( title.data(), flags ) ) ImChieldsBase::draw();
}
