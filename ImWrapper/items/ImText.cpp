#include "ImText.h"

ImText::ImText( std::string_view str ) : ImWithText( str ) {}

void ImText::draw() {
	ImGui::Text( "%s", text.data() );
	processEvent();
	processTooltip();
}
