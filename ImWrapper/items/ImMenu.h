#ifndef IMMENU_H
#define IMMENU_H

#include "../components/ImWithText.h"
#include "../components/ImWithWindowFlags.h"
#include "../ImChieldsBase.h"
#include <atomic>

class ImMenu : public ImChieldsBase, public ImWithText, public ImWithWindowFlags
{
	size_t _ImGuiDraw;

	ImVec2 _size;
	std::atomic_bool _sizeUsed = true;
	ImVec2 _pos;
	ImVec2 _pivot;
	std::atomic_bool _posUsed = true;
	bool _collapsed;
	std::atomic_bool _collapsedUsed = true;
	bool _focus;
	std::atomic_bool _focusUsed = true;
	bool _show = false;
public:
	ImMenu( std::string_view str );
	virtual ~ImMenu();

	virtual void setSize(const ImVec2 &size);
	virtual void setPos(const ImVec2& pos, const ImVec2& pivot = ImVec2(0,0));
	virtual void setCollapsed(bool collapsed);
	virtual void setFocus();

	virtual ImVec2 size();
	virtual ImVec2 pos();
	virtual bool isCollapsed();
	virtual bool isFocused();

	virtual void show();
	virtual void hide();
	virtual void toggle(bool showed);

	virtual bool isShowed();
	virtual bool isHidden();

	bool closable = true;

	SRSignal<ImVec2> onResize;
	SRSignal<ImVec2> onMove;
	SRSignal<bool> onCollaps;
	SRSignal<bool> onFocus;
	SRSignal<> onShow;
	SRSignal<> onHide;
	SRSignal<bool> onToggle;

protected:
	void draw() override;
};

#endif // IMMENU_H
