#ifndef IMBUTTON_H
#define IMBUTTON_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithSize.h"
#include "../components/ImWithText.h"
#include "../components/ImWithTooltip.h"

class ImButton : public ImBase,
				 public ImWithSize,
				 public ImWithText,
				 public ImWithAllEvents,
				 public ImWithTooltip {
public:
	ImButton( std::string_view str );

	SRSignal<> onClick;

protected:
	void draw() override;
};

#endif // IMBUTTON_H
