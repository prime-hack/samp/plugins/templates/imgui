#include "ImSeparator.h"

void ImSeparator::draw() {
	ImGui::Separator();
	processEvent();
	processTooltip();
}
