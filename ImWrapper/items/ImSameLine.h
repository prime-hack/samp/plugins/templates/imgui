#ifndef IMSAMELINE_H
#define IMSAMELINE_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithTooltip.h"
#include "../components/ImWithTooltip.h"

class ImSameLine : public ImBase, public ImWithAllEvents, public ImWithTooltip
{
public:
	float pos_x;
	float spacing_w;

protected:
	void draw() override;
};

#endif // IMSAMELINE_H
