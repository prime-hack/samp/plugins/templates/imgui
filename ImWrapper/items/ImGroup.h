#ifndef IMGROUP_H
#define IMGROUP_H

#include "../ImChieldsBase.h"
#include "../components/ImWithAllEvents.h"

class ImGroup : public ImChieldsBase, public ImWithAllEvents
{
protected:
	void draw() override;
};

#endif // IMGROUP_H
