#ifndef IMCHECKBOX_H
#define IMCHECKBOX_H

#include "../ImBase.h"
#include "../components/ImWithText.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithTooltip.h"

class ImCheckBox : public ImBase, public ImWithText, public ImWithAllEvents, public ImWithTooltip {
	bool *_pChecked = nullptr;
	bool  _checked	= false;

public:
	ImCheckBox( std::string_view str, bool checked = false );
	ImCheckBox( std::string_view str, bool *pChecked );

	virtual void setPointer( bool *pChecked );

	bool checked   = false;
	bool checkable = true;

	SRSignal<>		 onChecked;
	SRSignal<>		 onUnchecked;
	SRSignal< bool > onToggle;

protected:
	void		 draw() override;
	virtual void emitChange();
};

#endif // IMCHECKBOX_H
