#ifndef IMDUMMY_H
#define IMDUMMY_H

#include "../ImBase.h"
#include "../components/ImWithSize.h"

class ImDummy : public ImBase, public ImWithSize
{
public:
	ImDummy(const ImVec2 &sz = {0,0});
	ImDummy(const float &x, const float &y);

protected:
	void draw() override;
};

#endif // IMDUMMY_H
