#include "ImInputInt.h"

ImInputInt::ImInputInt( std::string_view name, const int &v ) : _value( v ), name( name ) {}

ImInputInt::ImInputInt( std::string_view name, int *v ) : _pValue( v ), name( name ) {}

void ImInputInt::setPointer( int *pValue ) {
	_pValue = pValue;
}

int ImInputInt::value() {
	return _value;
}

void ImInputInt::setValue( const int &v ) {
	_value = v;
	onIntChanged( v );
}

void ImInputInt::draw() {
	int v = _value;
	ImGui::InputInt( name.data(), ( _pValue ? _pValue : &_value ), step, fastStep, flags );
	if ( ( _pValue ? *_pValue : _value ) <= min ) ( _pValue ? *_pValue : _value ) = min;
	if ( ( _pValue ? *_pValue : _value ) >= max ) ( _pValue ? *_pValue : _value ) = max;
	if ( _pValue ) _value = *_pValue;
	if ( v != _value ) {
		onIntEdited( _value );
		onIntChanged( _value );
	}
	processEvent();
	processTooltip();
	if ( isActive() && isFocus() )
		if ( ImGui::IsKeyPressed( ImGuiKey_Enter, false ) ) onReturnPressed();
}
