#include "ImTextColored.h"
#include "../../split.hpp"
#include "ImDummy.h"
#include "ImSameLine.h"

namespace {
constexpr std::size_t str2int(std::string_view str, bool hex = true)
{
    std::size_t result = 0;
    for (std::size_t i = 0; i < str.length(); ++i) {
        if (str[i] >= '0' && str[i] <= '9') {
            result = result * (hex ? 0x10 : 10) + (str[i] - '0');
        } else if (hex && str[i] >= 'a' && str[i] <= 'f') {
            result = result * 0x10 + 0x0A + (str[i] - 'a');
        } else if (hex && str[i] >= 'A' && str[i] <= 'F') {
            result = result * 0x10 + 0x0A + (str[i] - 'A');
        }
    }
    return result;
}
}

ImTextColored::ImTextColored(std::string_view str, const size_t& clr)
    : ImText(str)
    , color(clr)
{
}

std::deque<ImBase*> ImTextColored::fromFormatedString(const std::string& str, size_t clr)
{
    std::deque<ImBase*> result;
    auto space = ImGui::CalcTextSize(" ").x;
    auto lines = string_split::split(str, "\n");
    for (auto& line : lines) {
        float lineSpace = 0;
        while (true) {
            auto carret = line.find('{');
            if (carret != std::string::npos && (line.length() >= carret + 7 || line.length() >= carret + 9)) {
                size_t colorLength = 0;
                if (line.at(carret + 7) == '}')
                    colorLength = 6;
                else if (line.at(carret + 9) == '}')
                    colorLength = 8;
                if (colorLength) {
                    auto isColor = [&]() -> bool {
                        for (size_t i = carret + 1; line.at(i) != '}'; ++i) {
                            if (line.at(i) >= '0' && line.at(i) <= '9')
                                continue;
                            if (line.at(i) >= 'a' && line.at(i) <= 'f')
                                continue;
                            if (line.at(i) >= 'A' && line.at(i) <= 'F')
                                continue;
                            return false;
                        }
                        return true;
                    };
                    if (isColor()) {
                        auto str = line.substr(0, carret);
                        if (!str.empty()) {
                            result.push_back(new ImTextColored(str, clr));
                            lineSpace += space;
                        }
                        auto color = line.substr(carret + 1, colorLength);
                        clr = str2int(color, true);
                        if (((uint8_t*)&clr)[0] == 0)
                            ((uint8_t*)&clr)[0] = 255;
                        line = line.substr(carret + colorLength + 2);
                        if (line.empty())
                            break;
                        if (!str.empty())
                            result.push_back(new ImSameLine());
                        if (lineSpace) {
                            result.push_back(new ImDummy(0, lineSpace));
                            result.push_back(new ImSameLine());
                        }
                    }
                }
            } else {
                if (!line.empty())
                    result.push_back(new ImTextColored(line, clr));
                break;
            }
        }
    }
    return result;
    }

void ImTextColored::draw()
{
    ImColor clr(((uint8_t*)&color)[1], ((uint8_t*)&color)[2], ((uint8_t*)&color)[3],
        ((uint8_t*)&color)[0]);
    ImGui::TextColored(clr, "%s", text.data());
    processEvent();
    processTooltip();
}
