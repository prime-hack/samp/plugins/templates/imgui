#ifndef IMCHIELD_H
#	define IMCHIELD_H

#include <string>

#include "../ImChieldsBase.h"
#include "../components/ImWithSize.h"
#include "../components/ImWithWindowFlags.h"
#include "../components/ImWithAllEvents.h"

class ImChield : public ImChieldsBase, public ImWithSize, public ImWithWindowFlags, public ImWithAllEvents {
	std::string _id;

public:
	ImChield( std::string_view id, const bool &border = false );

	bool border;

protected:
	void draw() override;
};

#endif // IMCHIELD_H
