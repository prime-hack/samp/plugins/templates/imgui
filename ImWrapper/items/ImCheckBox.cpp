#include "ImCheckBox.h"

ImCheckBox::ImCheckBox( std::string_view str, bool checked )
	: ImWithText( str ), checked( checked ), _checked( checked ) {}

ImCheckBox::ImCheckBox( std::string_view str, bool *pChecked )
	: ImWithText( str ), _pChecked( pChecked ), checked( *pChecked ), _checked( *pChecked ) {}

void ImCheckBox::setPointer( bool *pChecked ) {
	_pChecked = pChecked;
}

void ImCheckBox::draw() {
	if ( checked != _checked ) {
		_checked = checked;
		if ( _pChecked ) *_pChecked = checked;
		emitChange();
	} else if ( _pChecked && _checked != *_pChecked ) {
		_checked = checked = *_pChecked;
		emitChange();
	}
	_checked = checked;
	ImGui::Checkbox( text.data(), ( _pChecked ? _pChecked : &_checked ) );
	if ( _pChecked ) _checked = *_pChecked;
	processEvent();
	processTooltip();

	if ( _checked != checked ) {
		if ( checkable ) {
			checked = _checked;
			emitChange();
		} else {
			_checked = checked;
			if ( _pChecked ) *_pChecked = checked;
		}
	}
}

void ImCheckBox::emitChange() {
	onToggle( checked );
	if ( checked )
		onChecked();
	else
		onUnchecked();
}
