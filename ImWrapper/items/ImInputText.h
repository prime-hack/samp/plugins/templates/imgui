#ifndef IMINPUTTEXT_H
#define IMINPUTTEXT_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithTooltip.h"

class ImInputText : public ImBase, public ImWithAllEvents, public ImWithTooltip
{
	size_t _limit = 0;
	char *_buffer;
public:
	ImInputText( std::string_view name, std::string_view text = "", size_t limit = 1024 );
	virtual ~ImInputText();

	virtual void resize(size_t size);
	virtual std::string_view text();
	virtual void setText( std::string_view text );

	std::string name;
	ImGuiInputTextFlags flags = 0;

	SRSignal<std::string_view> onTextEdited;
	SRSignal<std::string_view> onTextChanged;
	SRSignal<> onReturnPressed;

protected:
	void draw() override;
};

#endif // IMINPUTTEXT_H
