#include "ImDragFloat.h"

ImDragFloat::ImDragFloat( std::string_view name, const float &v ) : _value( v ), name( name ) {}

ImDragFloat::ImDragFloat( std::string_view name, float *v ) : _pValue( v ), name( name ) {}

void ImDragFloat::setPointer( float *pValue ) { _pValue = pValue; }

float ImDragFloat::value() { return _value; }

void ImDragFloat::setValue( const float &v ) {
	if ( v < min )
		_value = min;
	else if ( v > max )
		_value = max;
	else
		_value = v;
	onFloatChanged( _value );
}

void ImDragFloat::draw() {
	float v = _value;
	ImGui::DragFloat( name.data(), ( _pValue ? _pValue : &_value ), speed, min, max, format.data(), power );
	if ( _pValue ) _value = *_pValue;
	if ( v != _value ) {
		onFloatDraged( _value );
		onFloatChanged( _value );
	}
	processEvent();
	processTooltip();
}
