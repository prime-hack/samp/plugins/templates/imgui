#include "ImChield.h"

ImChield::ImChield( std::string_view id, const bool &border )
	: _id( std::string( id ) + "\0" ), border( border ) {}

void ImChield::draw() {
	ImGui::BeginChild( _id.data(), size, border, flags );
	ImChieldsBase::draw();
	ImGui::EndChild();
	processEvent();
}
