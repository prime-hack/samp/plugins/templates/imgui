#ifndef IMCOLLAPS_H
#	define IMCOLLAPS_H

#include <string>

#include "../ImChieldsBase.h"
#include "../components/ImWithNodeFlags.h"

class ImCollaps : public ImChieldsBase, public ImWithNodeFlags {
public:
	ImCollaps( std::string_view title );
    
	std::string title;

protected:
	void draw() override;
};

#endif // IMCOLLAPS_H
