#ifndef IMINPUTFLOAT_H
#define IMINPUTFLOAT_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithTooltip.h"

class ImInputFloat : public ImBase, public ImWithAllEvents, public ImWithTooltip
{
	float _value;
	float *_pValue = nullptr;
public:
	ImInputFloat( std::string_view name, const float &v = 0 );
	ImInputFloat( std::string_view name, float *v );

	virtual void setPointer(float *pValue);

	virtual float value();
	virtual void setValue(const float &v);

	float min = -999999.999999;
	float max = 999999.999999;
	float step = 1;
	float fastStep = 100;
	std::string name;
	ImGuiInputTextFlags flags = 0;

	SRSignal<float> onFloatEdited;
	SRSignal<float> onFloatChanged;
	SRSignal<> onReturnPressed;

protected:
	void draw() override;
};

#endif // IMINPUTFLOAT_H
