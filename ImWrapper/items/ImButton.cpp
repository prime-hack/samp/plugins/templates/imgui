#include "ImButton.h"

ImButton::ImButton( std::string_view str ) : ImWithText( str ) {}

void ImButton::draw() {
	if ( ImGui::Button( text.data(), size ) ) onClick();
	processEvent();
	processTooltip();
}
