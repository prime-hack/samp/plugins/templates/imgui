#ifndef IMDRAWCUSTOM_H
#define IMDRAWCUSTOM_H

#include "../ImBase.h"

class ImDrawCustom : public ImBase
{
public:
	SRSignal<> onDraw;

protected:
	void draw() override;
};

#endif // IMDRAWCUSTOM_H
