#include "ImMenu.h"
#include <ImLoader.h>

ImMenu::ImMenu( std::string_view str ) : ImWithText( str ) {
	_ImGuiDraw = ImGui::ImLoader::get()->onDraw.connect( this, &ImMenu::draw );
}

ImMenu::~ImMenu() {
	ImGui::ImLoader::get()->onDraw -= _ImGuiDraw;
}

void ImMenu::setSize( const ImVec2 &size ) {
	_size	  = size;
	_sizeUsed = false;
}

void ImMenu::setPos( const ImVec2 &pos, const ImVec2 &pivot ) {
	_pos	 = pos;
	_pivot	 = pivot;
	_posUsed = false;
}

void ImMenu::setCollapsed( bool collapsed ) {
	_collapsed	   = collapsed;
	_collapsedUsed = false;
}

void ImMenu::setFocus() {
	_focusUsed = false;
}

ImVec2 ImMenu::size() {
	return _size;
}

ImVec2 ImMenu::pos() {
	return _pos;
}

bool ImMenu::isCollapsed() {
	return _collapsed;
}

bool ImMenu::isFocused() {
	return _focus;
}

void ImMenu::show() {
	_show = true;
	onShow();
}

void ImMenu::hide() {
	_show = false;
	onHide();
}

void ImMenu::toggle( bool showed ) {
	_show = showed;
	if ( _show )
		onShow();
	else
		onHide();
	onToggle( _show );
}

bool ImMenu::isShowed() {
	return _show;
}

bool ImMenu::isHidden() {
	return _show ^ true;
}

void ImMenu::draw() {
	if ( !_show ) return;

	if ( !_posUsed ) {
		_posUsed = true;
		ImGui::SetNextWindowPos( _pos, ImGuiCond_Always, _pivot );
	}
	if ( !_sizeUsed ) {
		_sizeUsed = true;
		ImGui::SetNextWindowSize( _size, ImGuiCond_Always );
	}
	if ( !_collapsedUsed ) {
		_collapsedUsed = true;
		ImGui::SetNextWindowCollapsed( _collapsed, ImGuiCond_Always );
	}
	if ( !_focusUsed ) {
		_focusUsed = true;
		ImGui::SetNextWindowFocus();
	}
	bool showBak  = _show;
	bool showMenu = _show;
	if ( !closable )
		ImGui::Begin( text.data(), nullptr, flags );
	else
		ImGui::Begin( text.data(), &showMenu, flags );
	ImChieldsBase::draw();
	if ( _posUsed ) {
		auto newPos = ImGui::GetWindowPos();
		if ( _pos.x != newPos.x || _pos.y != newPos.y ) onMove( newPos );
		_pos = newPos;
	}
	if ( _sizeUsed ) {
		auto newSize = ImGui::GetWindowSize();
		if ( _size.x != newSize.x || _size.y != newSize.y ) onResize( newSize );
		_size = newSize;
	}
	if ( _collapsedUsed ) {
		if ( _collapsed != ImGui::IsWindowCollapsed() ) onCollaps( ImGui::IsWindowCollapsed() );
		_collapsed = ImGui::IsWindowCollapsed();
	}
	if ( _focus != ImGui::IsWindowFocused() ) onFocus( ImGui::IsWindowFocused() );
	_focus = ImGui::IsWindowFocused();
	ImGui::End();
	if ( closable && showMenu != _show ) {
		if ( showBak == _show ) _show = showMenu;
		onToggle( _show );
		onHide();
	}
}
