#include "ImNode.h"

ImNode::ImNode( std::string_view title ) : title( std::string( title ) + "\0" ) {}

void ImNode::draw() {
	if ( ImGui::TreeNode( title.data() ) ) {
		ImChieldsBase::draw();
		ImGui::TreePop();
	}
}
