#include "ImInputFloat.h"

ImInputFloat::ImInputFloat( std::string_view name, const float &v ) : _value( v ), name( name ) {}

ImInputFloat::ImInputFloat( std::string_view name, float *v ) : _pValue( v ), name( name ) {}

void ImInputFloat::setPointer( float *pValue ) {
	_pValue = pValue;
}

float ImInputFloat::value() {
	return _value;
}

void ImInputFloat::setValue( const float &v ) {
	_value = v;
	onFloatChanged( v );
}

void ImInputFloat::draw() {
	float v = _value;
	ImGui::InputFloat( name.data(), ( _pValue ? _pValue : &_value ), step, fastStep, "%.2f", flags );
	if ( ( _pValue ? *_pValue : _value ) <= min ) ( _pValue ? *_pValue : _value ) = min;
	if ( ( _pValue ? *_pValue : _value ) >= max ) ( _pValue ? *_pValue : _value ) = max;
	if ( _pValue ) _value = *_pValue;
	if ( v != _value ) {
		onFloatEdited( _value );
		onFloatChanged( _value );
	}
	processEvent();
	processTooltip();
	if ( isActive() && isFocus() )
		if ( ImGui::IsKeyPressed( ImGuiKey_Enter, false ) ) onReturnPressed();
}
