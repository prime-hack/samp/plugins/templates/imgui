#ifndef IMNODE_H
#	define IMNODE_H

#include <string>

#include "../ImChieldsBase.h"

class ImNode : public ImChieldsBase {
public:
	ImNode( std::string_view title );
    
	std::string title;

protected:
	void draw() override;
};

#endif // IMNODE_H
