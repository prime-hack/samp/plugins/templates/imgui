#ifndef IMTEXT_H
#define IMTEXT_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithText.h"
#include "../components/ImWithTooltip.h"

class ImText : public ImBase, public ImWithText, public ImWithAllEvents, public ImWithTooltip {
public:
	ImText( std::string_view str );

protected:
	void draw() override;
};

#endif // IMTEXT_H
