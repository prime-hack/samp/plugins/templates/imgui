#ifndef IMDRAGFLOAT_H
#define IMDRAGFLOAT_H

#include "../ImBase.h"
#include "../components/ImWithAllEvents.h"
#include "../components/ImWithTooltip.h"

class ImDragFloat : public ImBase, public ImWithAllEvents, public ImWithTooltip
{
	float _value;
	float *_pValue = nullptr;
public:
	ImDragFloat( std::string_view name, const float &v = 0.0f );
	ImDragFloat( std::string_view name, float *v );

	virtual void setPointer(float *pValue);

	virtual float value();
	virtual void setValue(const float &v);

	std::string name;
	float speed = 1.0f;
	float min = 0.0f;
	float max = 1.0f;
	std::string format = "%.3f";
	float power = 1.0f;

	SRSignal<float> onFloatDraged;
	SRSignal<float> onFloatChanged;

protected:
	void draw() override;
};

#endif // IMDRAGFLOAT_H
