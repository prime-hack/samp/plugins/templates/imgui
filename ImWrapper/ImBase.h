#ifndef IMBASE_H
#define IMBASE_H

#include <SRSignal/SRSignal.hpp>
#include <imgui.h>

class ImBase {
	friend class ImChieldsBase;
	bool __base_drawable = true;

public:
	virtual ~ImBase() {}

	virtual bool drawable();
	virtual void setDrawable( bool );

	SRSignal<bool> onToggleDrawable;

protected:
	virtual void draw() = 0;
};

#endif // IMBASE_H
