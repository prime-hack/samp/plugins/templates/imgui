#include "ImChieldsBase.h"

void ImChieldsBase::draw() {
	for ( const auto &chield : chields ) {
		if ( chield == nullptr ) continue;
		if ( chield->drawable() ) chield->draw();
	}
}
