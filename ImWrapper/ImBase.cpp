#include "ImBase.h"

bool ImBase::drawable() {
	return __base_drawable;
}

void ImBase::setDrawable( bool d ) {
	__base_drawable = d;
	onToggleDrawable( d );
}
